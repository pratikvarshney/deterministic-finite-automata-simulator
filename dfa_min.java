import java.io.*;
import java.util.*;

//Q,q0,F,E,&
class dfa_min
{
    public static void main(String[] args)
    {
        try{
            Scanner sc = new Scanner(System.in);
            BufferedReader br;
            String str;
            if(args.length==0) 
            {
                System.out.print("Enter Input file path: ");
                str=sc.next();
            }
            else str=args[0];
            br = new BufferedReader(new InputStreamReader(new FileInputStream(str)));
            StringTokenizer st;
            
            //Read Q
            str = br.readLine();
            String[] Q;
            Q=str.split(",");

            //Read q0
            String q0 = br.readLine();

            //Read F
            str = br.readLine();
            String[] F;
            F=str.split(",");
            
            //Read E
            str = br.readLine();
            st = new StringTokenizer(str,",");
            String[] E;
            E=new String[st.countTokens()-1];
            E[0]=st.nextToken(); //# will be skipped/overwritten
            for(int i=0; st.hasMoreTokens(); i++)
            {
                E[i]=st.nextToken();
            }
            
            //Read &
            String[][] d=new String[Q.length][E.length+1];
            for(int i=0; i<Q.length; i++)
            {
                str = br.readLine();
                d[i]=str.split(",");
            }
            
            //DISPLAY
            System.out.print("Q : ");
            if(Q.length>0) System.out.print(Q[0]);
            for(int i=1; i<Q.length; i++)
            {
                System.out.print(","+Q[i]);
            }
            System.out.println();
            
            System.out.print("E : ");
            if(E.length>0) System.out.print(E[0]);
            for(int i=1; i<E.length; i++)
            {
                System.out.print(","+E[i]);
            }
            System.out.println();
            
            System.out.print("F : ");
            if(F.length>0) System.out.print(F[0]);
            for(int i=1; i<F.length; i++)
            {
                System.out.print(","+F[i]);
            }
            System.out.println();
            
            System.out.println("q0 : "+q0);
            
            System.out.println("Transition Table : ");
            System.out.print("  ");
            if(E.length>0) System.out.print(E[0]);
            for(int i=1; i<E.length; i++)
            {
                System.out.print(","+E[i]);
            }
            System.out.println();
            for(int i=0; i<d.length; i++)
            {
                if(d[i].length>0) System.out.print(d[i][0]);
                for(int j=1; j<d[i].length; j++)
                {
                    System.out.print(","+d[i][j]);
                }
                System.out.println();
            }
            System.out.println();
            
            String[] QmF=new String[Q.length-F.length];
            int j,i;
            j=0;
            for(i=0; i<Q.length; i++)
            {
                if(indexOfStr(F, Q[i])==-1)
                {
                    QmF[j]=Q[i];
                    j++;
                }
            }
            
            System.out.print("Q-F : ");
            if(QmF.length>0) System.out.print(QmF[0]);
            for(i=1; i<QmF.length; i++)
            {
                System.out.print(","+QmF[i]);
            }
            System.out.println();
            
            //CREATE TABLE
            String[][] table;
            table = new String[Q.length-1][];
            for(i=0; i<table.length; i++)
            {
                table[i]=new String[i+1];
                for(j=0; j<table[i].length; j++)
                {
                    table[i][j]=".";
                }
            }
            
            for(i=0; i<F.length; i++)
            {
                for(j=0; j<QmF.length; j++)
                {
                    mark(Q,table,F[i],QmF[j]);
                }
            }
            
            //For (Q-F) x (Q-F) and F x F
            int k, flag, m;
            String r,s;
            String[] abc;
            ArrayList list=new ArrayList();
            ArrayList group=new ArrayList();
            group.add(F);
            group.add(QmF);
            for(int elm=0; elm<group.size(); elm++)
            {
                abc=(String[])group.get(elm);
                for(i=0; i<abc.length-1; i++)
                {
                    for(j=i+1; j<abc.length; j++)
                    {
                        flag=0;
                        for(k=0; k<E.length; k++)
                        {
                            r=delta(d,E,abc[i],E[k]);
                            s=delta(d,E,abc[j],E[k]);
                            if(!(r.equals(s)))
                            {
                                if(isMarked(Q,table,r,s))
                                {
                                    mark(Q,table,abc[j],abc[i]);
                                    recMark(Q,table,abc[j],abc[i],list);
                                    flag=1;
                                }
                            }

                        }
                        if(flag==0)
                        {
                            for(k=0; k<E.length; k++)
                            {
                                r=delta(d,E,abc[i],E[k]);
                                s=delta(d,E,abc[j],E[k]);
                                int flag2;
                                if(!(r.equals(s)))
                                {
                                    flag2=0;
                                    for(m=0; m<list.size(); m++)
                                    {
                                        MyList temp;
                                        temp=((MyList)list.get(m));
                                        if((temp.p.equals(r) && temp.q.equals(s)) || (temp.q.equals(r) && temp.p.equals(s)))
                                        {
                                            temp.list.add(new MyList(abc[i],abc[j]));
                                            flag2=1;
                                            break;
                                        }
                                    }
                                    if(flag2==0)
                                    {
                                        list.add(new MyList(r,s));
                                        ((MyList)list.get(list.size()-1)).list.add(new MyList(abc[i],abc[j]));
                                    }

                                }

                            }
                        }
                    }
                }
            }
            
            System.out.println();
            showTable(Q, table);
            showEqStates(Q,table);
            EqClass curr = null;
            ArrayList states = new ArrayList();
            for(i=0; i<Q.length; i++)
            {
                flag=0;
                for(k=0; k<states.size(); k++)
                {
                    curr = (EqClass)states.get(k);
                    for(int l=0; l<curr.list.size(); l++)
                    {
                        if(Q[i].equals((String)curr.list.get(l)))
                        {
                            flag=1;
                            break;
                        }
                    }
                    if(flag==1) break;
                }
                if(flag==0)
                {
                    states.add(new EqClass());
                    curr = (EqClass)states.get(states.size()-1); 
                    curr.list.add(Q[i]);
                }
                for(j=i+1; j<Q.length; j++)
                {
                    if(!(isMarked(Q, table, Q[i], Q[j]))) curr.list.add(Q[j]);
                }
            }
            System.out.println();
            int max_label_len=0;
            System.out.println("New set of States :");
            for(i=0; i<states.size(); i++)
            {
                curr = (EqClass)states.get(i);
                str="["+(String)curr.list.get(0);
                for(j=1; j<curr.list.size(); j++)
                {
                    str=str+","+(String)curr.list.get(j);
                }
                str=str+"]";
                curr.label=str;
                System.out.println(curr.label);
                max_label_len=(str.length()>max_label_len)?str.length():max_label_len;
            }
            
            System.out.println();
            System.out.println("Transition Table: ");
            System.out.println();
            
            //max_label_len++;
            String pattern="%-"+max_label_len+"s";
            System.out.printf(pattern, " ");
            for(i=0; i<E.length; i++)
            {
                System.out.printf("  %c  ", 179);
                System.out.printf(pattern, E[i]);
            }
            System.out.println();
            for(i=-2; i<max_label_len; i++)
            {
                System.out.printf("%c", 196);
            }
            for(i=0; i<E.length; i++)
            {
                System.out.printf("%c", 197);
                for(j=-4; j<max_label_len; j++)
                    System.out.printf("%c", 196);
            }
            for(i=0; i<states.size(); i++)
            {
                curr = (EqClass)states.get(i);
                System.out.println();
                System.out.printf(pattern, curr.label);
                for(j=0; j<E.length; j++)
                {
                    System.out.printf("  %c  ", 179);
                    System.out.printf(pattern, getLabel(states, delta(d,E,(String)curr.list.get(0), E[j])));
                }
            }
            System.out.println("\n");
            
        }catch(Exception e)
        {
            System.out.println(e);
        }
    }
    
    public static int indexOfStr(String[] arr, String str)
    {
        int i;
        for(i=0; i<arr.length; i++)
        {
            if(arr[i].equals(str))
            {
                return i;
            }
        }
        return -1;
    }
    
    public static String[][] mark(String[] Q, String table[][], String p, String q)
    {
        int row, col, temp;
        row = indexOfStr(Q, p);
        col = indexOfStr(Q, q);
        if(row<col)
        {
            temp=row;
            row=col;
            col=temp;
        }
        row=row-1;
        table[row][col]="x";
        return table;
    }
    
    public static String[][] recMark(String[] Q, String table[][], String r, String s, ArrayList list)
    {
        int m,n;
        String a,b;
        for(m=0; m<list.size(); m++)
        {
            MyList temp, curr;
            temp=((MyList)list.get(m));
            if((temp.p.equals(r) && temp.q.equals(s)) || (temp.q.equals(r) && temp.p.equals(s)))
            {
                for(n=0; n<temp.list.size(); n++)
                {
                    curr=(MyList)temp.list.get(n);
                    mark(Q,table,curr.p, curr.q);
                    recMark(Q,table,curr.p, curr.q, curr.list);
                }
            }
        }
        return table;
    }
    
    public static boolean isMarked(String[] Q, String table[][], String p, String q)
    {
        int row, col, temp;
        row = indexOfStr(Q, p);
        col = indexOfStr(Q, q);
        if(row<col)
        {
            temp=row;
            row=col;
            col=temp;
        }
        row=row-1;
        return table[row][col].equals("x");
    }
    
    public static void showTable(String[] Q, String[][] table)
    {
        int i,j;
        System.out.println("Minimization Table: ");
        for(i=0; i<table.length; i++)
        {
            System.out.print(Q[i+1]);
            for(j=0; j<table[i].length; j++)
            {
                System.out.print(" "+table[i][j]);
            }
            System.out.println();
        }
        System.out.print(" ");
        for(i=0; i<Q.length-1; i++)
        {
            System.out.print(" "+Q[i]);
        }
        System.out.println();
    }
    
    public static void showEqStates(String[] Q, String[][] table)
    {
        int i,j;
        System.out.println();
        System.out.println("Equivalent States: ");
        for(i=0; i<table.length; i++)
        {
            for(j=0; j<table[i].length; j++)
            {
                if(!(table[i][j].equals("x")))
                {
                    System.out.println("("+Q[j]+","+Q[i+1]+")");
                }
            }
        }
    }
    
    public static String delta(String[][] d, String[] E, String p, String a)
    {
        int index;
        index=indexOfStr(E,a)+1;
        for(int i=0; i<d.length; i++)
        {
            if(d[i][0].equals(p)) return d[i][index];
        }
        return "";
    }
    
    public static String getLabel(ArrayList states, String q)
    {
        EqClass curr=null;
        for(int i=0; i<states.size(); i++)
        {
            curr = (EqClass)states.get(i);
            for(int j=0; j<curr.list.size(); j++)
            {
                if(q.equals((String)curr.list.get(j))) return curr.label;
            }
        }
        return "";
    }
}

class MyList
{
    String p, q;
    ArrayList list;
    public MyList(String x, String y)
    {
        p=x;
        q=y;
        list=new ArrayList();
    }
}

class EqClass
{
    String label;
    ArrayList list;
    public EqClass()
    {
        list=new ArrayList();
    }
}