# README #

* Deterministic Finite Automata simulator
* Output:
    * Minimization Table
    * Equivalent States
    * New set of States
    * Transition Table

### INPUT ###
* Q : a finite set of states.
* q0: the initial state from where any input is processed (belongs to Q)
* F : a set of final state/states of Q (subset of Q)
* E : a finite set of symbols called the alphabet.
* & : the transition function where &: Q x E -> Q

### Example ###
```
#!html

Q : a,b,c,d,e,f,g,h 
E : 0,1
F : c
q0 : a
Transition Table :
  0,1
a,b,f
b,g,c
c,a,c
d,c,g
e,h,f
f,c,g
g,b,f
h,g,c

Q-F : a,b,d,e,f,g,h

Minimization Table:
b x
c x x
d x x x
e . x x x
f x x x . x
g . x x x . x
h x . x x x x x
  a b c d e f g

Equivalent States:
(a,e)
(d,f)
(a,g)
(e,g)
(b,h)

New set of States :
[a,e,g]
[b,h]
[c]
[d,f]

Transition Table:

         |  0        |  1
----------------------------------
[a,e,g]  |  [b,h]    |  [d,f]
[b,h]    |  [a,e,g]  |  [c]
[c]      |  [a,e,g]  |  [c]
[d,f]    |  [c]      |  [a,e,g]

```